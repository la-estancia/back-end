using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AppWebRestaurante.BackEnd
{
    public class ClsMesas
    {
        private static ClsMesas instancia;
        ClsConexion conectar = ClsConexion.getInstancia();

        public static ClsMesas getInstancia()
        {
            if(instancia == null)
            {
                instancia = new ClsMesas();
            }
            return instancia;
        }

        public DataTable consultarMesas()
        {            
            return conectar.hacerConsulta(" select a.codigoMesa,  " +
                                         "  a.DescripcionMesa, " +
                                         " 	case a.codigoEstadoMesa   when 1 then 'Disponible'   when 2 then 'Ocupado'  when 3 then 'Por Desocupar'  END   as Estado,  " +
                                         " 	case a.codigoEstadoMesa   when 1 then 'btn btn-success'  when 2 then 'btn btn-danger'   when 3 then 'btn btn-primary'  END  as Estilo,  " +
                                         " 	case a.codigoEstadoMesa when 1 then '-' when 2 then 'DESOCUPAR MESA' when 3 then 'DESOCUPAR MESA' END as Visiblex, " +
                                         " 	isnull(c.NombreUsuario, 'Sin mesero asignado') as NombreUsuario " +
                                         " from mesa a " +
                                         " left outer join PEDIDO b on a.CodigoMesa = b.CodigoMesa and b.Estado = 1 " +
                                         " left outer join USUARIO c on b.CodigoUsuario = c.CodigoUsuario ");
        }                 

        public DataTable consultarMesero()
        {
            //return conectar.hacerConsulta("select * from USUARIO where CodigoUsuario not in (select distinct codigoUsuario from pedido where Estado = 1) ");
            return conectar.hacerConsulta("select * from USUARIO ");
        }

        public DataTable consultarProductos()
        {
            return conectar.hacerConsulta("SELECT * FROM PRODUCTO");
        }

        public DataTable consultarCombos()
        {
            return conectar.hacerConsulta("SELECT * FROM COMBOHEAD");
        }

    }
}