using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AppWebRestaurante.BackEnd
{
    public class ClsPedidos
    {
        private static ClsPedidos instancia;
        private ClsConexion conectar = ClsConexion.getInstancia();
        public static ClsPedidos getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ClsPedidos();
            }
            return instancia;
        }

        public DataTable consultarCaja()
        {
            return conectar.hacerConsulta(" select * from caja ");
        }
        public DataTable consultarPedidoMesa(int codigoPedido)
        {
            return conectar.hacerConsulta(" select a.CodigoPedido, a.CodigoPedidoDetalle, a.codigoProducto, a.codigoCombo,  " + 
                                          " case a.codigoProducto when 0 then d.DescripcionCombo else c.DescripcionProducto  end as Nombre,  " +
                                          " a.cantidad, " +
                                          " case a.CodigoCombo when 0 then c.PrecioVenta else d.PrecioCombo  end as Precio " +
                                          " from PEDIDODETALLE a  " +
                                          " inner join PEDIDO b on a.CodigoPedido = B.CodigoPedido " +
                                          " inner join PRODUCTO c on a.CodigoProducto = c.CodigoProducto " +
                                          " inner join COMBOHEAD d on a.CodigoCombo = d.CodigoCombo " + 
                                          " where b.CodigoMesa = "+ codigoPedido +  " and b.Estado = 1 ");
        }

        public DataTable consultarPedidoMesaFactura(int codigoPedido)
        {
            return conectar.hacerConsulta(" select a.CodigoPedido, a.CodigoPedidoDetalle, a.codigoProducto, a.codigoCombo,  " +
                                          " case a.codigoProducto when 0 then d.DescripcionCombo else c.DescripcionProducto  end as Nombre,  " +
                                          " a.cantidad, " +
                                          " case a.CodigoCombo when 0 then c.PrecioVenta else d.PrecioCombo  end as Precio " +
                                          " from PEDIDODETALLE a  " +
                                          " inner join PEDIDO b on a.CodigoPedido = B.CodigoPedido " +
                                          " inner join PRODUCTO c on a.CodigoProducto = c.CodigoProducto " +
                                          " inner join COMBOHEAD d on a.CodigoCombo = d.CodigoCombo " +
                                          " where b.CodigoMesa = " + codigoPedido);
        }

        public void crearPedido(int codigoMesero, int codigoMesa)
        {
            conectar.ejecutarQuery(" DECLARE @CodigoPedido as INT " +
                                   " SET @CodigoPedido = (select isnull(max(codigoPedido),0) + 1 from PEDIDO) " +
                                   " INSERT INTO PEDIDO (CodigoPedido, CodigoUsuario, CodigoMesa, Fecha, TotalPedido, Descuento, Estado, despachado) " +
                                   " values (@CodigoPedido, " + codigoMesero + ", " + codigoMesa + ", getdate(), 0, 0, 1, 0) " ); 
        }

        public void crearDetallePedido(int codigoPedido, int codigoProducto, int codigoCombo, int cantidad)
        {
            conectar.ejecutarQuery(" DECLARE @CodigoPedidoDetalle as INT " +
                                   " SET @CodigoPedidoDetalle = (select isnull(max(CodigoPedidoDetalle),0) + 1 from PEDIDODETALLE where COdigoPedido = " + codigoPedido +") " +
                                   " INSERT INTO PEDIDODETALLE (CodigoPedido, CodigoPedidoDetalle, CodigoProducto, CodigoCombo, cantidad)  " +
                                   " values ("+ codigoPedido +", @CodigoPedidoDetalle, " + codigoProducto + ", " + codigoCombo + ", " +cantidad + ") ");
        }

        public void quitarProducto(int codigoPedido, int codigoPedidoDetalle)
        {
            conectar.ejecutarQuery("DELETE FROM PEDIDODETALLE WHERE CodigoPedido = " + codigoPedido + " and codigoPedidoDetalle = " + codigoPedidoDetalle ); 

        }

        public void cancelarPedido(int codigoPedido, int codigoMesa)
        {
            conectar.ejecutarQuery("DELETE FROM PEDIDODETALLE WHERE CodigoPedido = " + codigoPedido);
            conectar.ejecutarQuery("DELETE FROM PEDIDO WHERE CodigoPedido = " + codigoPedido);
            conectar.ejecutarQuery("UPDATE MESA SET CodigoEstadoMesa = 1 WHERE CODIGOMESA = " + codigoMesa);
        }


        public DataTable obtenerPedidoMesa(int codigoMesa)
        {
            return conectar.hacerConsulta(" select * from PEDIDO where CodigoMesa = " + codigoMesa + " and Estado = 1 ");
        }

        public DataTable obtenerPedidoCocina()
        {
            return conectar.hacerConsulta(" select  a.codigoPedido, 'Mesa No :  ' + convert(varchar(100), CodigoMesa) as Mesa, b.NombreUsuario, fecha from pedido a inner join usuario b on a.codigoUsuario = b.codigoUsuario where a.despachado = 0 "); 
        }

        public DataTable obtenerPedidoCocina2()
        {
            return conectar.hacerConsulta(" select  a.codigoPedido, 'Mesa No :  ' + convert(varchar(100), CodigoMesa) as Mesa, b.NombreUsuario, fecha, case a.estado when 1 then 'Activo' when 0 then 'FInalizado' end as EstadoPedido from pedido a inner join usuario b on a.codigoUsuario = b.codigoUsuario");
        }

        public DataTable obtenerPedidoCocinaDetalle(int codigoPedido)
        {
            return conectar.hacerConsulta(" select a.CodigoPedido, a.CodigoPedidoDetalle, a.codigoProducto, a.codigoCombo, case a.codigoProducto when 0 then d.DescripcionCombo else c.DescripcionProducto  end as Nombre,   a.cantidad, case a.CodigoCombo when 0 then c.PrecioVenta else d.PrecioCombo  end as Precio from PEDIDODETALLE a inner join PEDIDO b on a.CodigoPedido = B.CodigoPedido inner join PRODUCTO c on a.CodigoProducto = c.CodigoProducto inner join COMBOHEAD d on a.CodigoCombo = d.CodigoCombo  where b.codigoPedido = " + codigoPedido);
        }

        public void actualizarDespacho(int codigoPedido)
        {
            conectar.ejecutarQuery("UPDATE PEDIDO SET Despachado = 1 WHERE CodigoPedido = " + codigoPedido);
        }
    }
}