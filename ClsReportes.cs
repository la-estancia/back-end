using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AppWebRestaurante.BackEnd
{
    public class ClsReportes
    {
        private ClsConexion conectar = ClsConexion.getInstancia();
        private static ClsReportes instancia;
        public static ClsReportes getInstancia()
        {
            if (instancia== null)
            {
                instancia = new ClsReportes();
            }
            return instancia;
        }

        public DataTable reporteIngresos()
        {
            return conectar.hacerConsulta(" select  " +
                                          " 		'Factura NO. ' + convert(varchar(100), a.codigoFactura) as NumeroFactura, " +
                                          " 		a.Nit, " +
                                          " 		a.TotalFactura, " +
                                          " 		case a.codigoMediaPago when 1 then 'Efectivo' else 'Tarjeta' end as MediaPago, " +
                                          " 		c.NombreUsuario, " +
                                          " 		a.FechaFactura " +
                                          " from FACTURA a  " +
                                          " INNER JOIN PEDIDO b on a.CodigoPedido = b.CodigoPedido  " +
                                          " INNER JOIN USUARIO C ON b.CodigoUsuario = c.CodigoUsuario ");
        }

        public DataTable reproteProductos()
        {
            return conectar.hacerConsulta(" select CodigoProducto, DescripcionProducto, PrecioVenta from PRODUCTO WHERE CodigoProducto > 0 ");
        }

        public DataTable reporteCombos()
        {
            return conectar.hacerConsulta(" select CodigoCombo, DescripcionCombo, PrecioCombo from COMBOHEAD WHERE CodigoCombo > 0 "); 
        }

        public DataTable reporteCombosDetalle(int codigoCombo)
        {
            return conectar.hacerConsulta(" select a.CodigoProducto, b.DescripcionProducto, a.Cantidad from COMBODETALLE a  inner join PRODUCTO b on a.CodigoProducto = b.CodigoProducto and a.CodigoCombo =  " + codigoCombo);
        }

        public DataTable reporteUsuarios()
        {
            return conectar.hacerConsulta(" select a.CodigoUsuario, a.NombreUsuario, a.login, b.NombreRol from usuario a inner join ROLUSUARIO b on a.CodigoRol = b.CodigoRol ");
        }
        
        public DataTable reporteIngresoMediaPago()
        {
            return conectar.hacerConsulta(" select case CodigoMediaPago when 1 then' Efectivo' when 2 then 'Tarjeta' end as MediaPago, sum(TotalFactura) as Total from FACTURA group by CodigoMediaPago ");
        }

        public DataTable estadisticaVentas(String año)
        {
            return conectar.hacerConsulta(" exec estadisticaVentas @Año = '" + año + "' ");
        }

        public DataTable estadisticaProductos(String año, String mes)
        {
            return conectar.hacerConsulta(" exec Estadisticaproductos @Año = '" + año + "', @Mes =  '"+ mes + "'" );
        }


    }
}